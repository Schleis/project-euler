class polygonalNumberSet(object):
	def getRange(self, polygonalNum, first, last, n=1):
	'''Get all the polygonal numbers within the range'''
		returnSet = []
		p = polygonalNum.getNumber(n)

		while p < last:
			if p > first:
				returnSet.append(p)
			p = polygonalNum.getNumber(++n)
		
		return returnSet

class polygonalNumber:
	def __init__(self, n=1):
		self.__n = n

	def getNumber(self):
		raise NotImplementedError

class SquareNumber(polygonalNumber):
	def getNumber(self, n):
		return n**2

class TriangleNumber(polygonalNumber):
	def getNumber(self, n):
		return (n*(n+1))/2

class PentagonalNumber(polygonalNumber):
	def getNumber(self, n):
		return (n*(3*n - 1))/2

class HexagonalNumber(polygonalNumber):
	def getNumber(self, n):
		return n*(2*n - 1)

class HeptagonalNumber(polygonalNumber):
	def getNumber(self, n):
		return (n*(5*n-3))/2

class OctoganalNumber(polygonalNumber):
	def getNumber(self, n):
		return n*(3*n-2)
