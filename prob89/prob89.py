'''
The rules for writing Roman numerals allow for many ways of writing each number.  However there is always a "best" way of writing a particular number.

For example the following represent all of the legitimate ways of writing the number sixteen:
	IIIIIIIIIIIIIIIII
	VIIIIIIIIIII
	VVIIIIII
	XIIIIII
	VVVI
	XVI

The last being considered the most efficient as it uses the least number of numerals.

The 11K text file, roman.txt contains one thousand numbers written in valid, but not necassilarly minimal, Roman Numerals.  That is they are arranged in descending units and obey the subtractive pair rule.

Find the number of characters saved by writing each of these in their minimal form.
'''


