import romannumerals
import unittest

class RomanTest(unittest.TestCase):
	knownValues = ( (1,'I'),
			(2, 'II'),
			(3, 'III'),
			(4, 'IV'),
			(5, 'V'),
			(6, 'VI'),
			(7, 'VII'),
			(8, 'VIII'),
			(9, 'IX'),
			(10, 'X'),
			(50, 'L'),
			(100, 'C'),
			(500, 'D'),
			(1000, 'M'),
			(31, 'XXXI'),
			(136, 'CXXXVI'),
			(39, 'XXXIX'),
			(49, 'XLIX'),
			(1043, 'MXLIII'),
			(3501, 'MMMDI'),
			(3999, 'MMMCMXCIX'),
		)
	
	notEfficent = (	(16, 'IIIIIIIIIIIIIIII'),
			(16, 'XIIIIII'),
			(16, 'VVVI'),
			(16, 'VVIIIIII'),
		)

	def testToRomanKnownValues(self):
		for value, numeral in self.knownValues:
			result = romannumerals.toRoman(value)
			self.assertEqual(numeral, result)

	def testFromRomanKnownValues(self):
		for value, numeral in self.knownValues:
			result = romannumerals.fromRoman(numeral)
			self.assertEqual(numeral, result)

	def testFromRomanNotEfficent(self):
		for value, numeral in self.notEfficent:
			result = romannumerals.fromRoman(numeral)
			self.assertEqual(value, result)

if __name__ == "__main__":
	unittest.main()
