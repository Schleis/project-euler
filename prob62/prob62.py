import itertools

'''
The cube, 41063625 (345^3), can be permuted to produce two other cubes: 56623104 (384^3) and 66430125 (405^3).  In fact, 41063625 is the smallest cube which has exactly three permutations of its digits which are also cube.

Find the smallest cube for which exactly five permutations of its digits are cube.
'''

def getCubesUntilNextMagnitude(n):
	'''Get a list of cubes until they increase in magnitude'''
	cube = n ** 3
	cubeLength = len(str(cube))

	cubes = []

	while cubeLength == len(str(cube)):
		cubes.append(cube)

		n = n + 1
		cube = n ** 3
	
	return {'cubes': cubes, 'nextN': n}

def sortCubeDigits(cubes):
	'''Sort a list of cubes by the digits that they contain'''
	
	sortCubes = {}

	for cube in cubes:
		digits = list(str(cube))
		digits.sort()
		sortedDigits = ''.join(digits)

		if sortedDigits in sortCubes:
			sortCubes[sortedDigits].append(cube)
			sortCubes[sortedDigits].sort()
		else:
			sortCubes[sortedDigits] = [cube]
	
	return sortCubes

def findSmallestCubePermutations(cubesList, smallestPermutations):
	'''Examine list of cubes find if any are of the smallestPermutations'''
	for digits in cubesList:
		numberOfPermutations = len(cubesList[digits])
		if numberOfPermutations in smallestPermutations:
			if cubesList[digits][0] < smallestPermutations[numberOfPermutations]:
				smallestPermutations[numberOfPermutations] = cubesList[digits][0]
		else:
			smallestPermutations[numberOfPermutations] = cubesList[digits][0]
	return smallestPermutations
	
smallestPermutations = {}
n = 1

while not (5 in smallestPermutations):
	cubes = getCubesUntilNextMagnitude(n)
	cubeList = sortCubeDigits(cubes['cubes'])
	smallestPermutations = findSmallestCubePermutations(cubeList, smallestPermutations)

	n = cubes['nextN']

print(smallestPermutations)
