"""The primes 3,7, 109, 673 are quite remarkable.
By takin any two primes and concantenating them in any order the result will always be prime.
For example, taking 7 ann 109, both 7109 and 1097 are prime.
The sum of these four primes, 792, represents the lowest for a set of four primes with this property.

Find the lowest sum for a set of five primes for which any two primes concatenate to produce another prime."""


"""Add the path to import the useful functions"""
import sys
sys.path.append('../')

import primes

primeCheck = primes.primes()
primeList = primeCheck.sieveErastothene(10100)
primeList.remove(2)
primeList.remove(5)

primeGroups = {}
smallestSum = 0

for prime in primeList:
	primeGroups[prime] = [prime]

for prime in primeList:
	for group in primeGroups.keys():
		thisGroup = list(primeGroups[group])
		allPrime = True
		for n in thisGroup:
			number1 = str(n) + str(prime)
			number2 = str(prime) + str(n)
			if(not primeCheck.isPrime(int(number1))) or (not primeCheck.isPrime(int(number2))):
				allPrime = False
			print(n, number1, number2, allPrime)
		if(allPrime):
			primeGroups[group].append(prime)
		print(prime, primeGroups[group], sum(primeGroups[group]))
		if(len(primeGroups[group]) == 5):
			exit()
