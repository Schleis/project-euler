import poker

cardFile = open('poker.txt', 'r')
winners = []
for line in cardFile:
	cardLine = line.rstrip('\r\n')
	pokerGame = poker.Game(cardLine.split(' '))
	winner = pokerGame.getWinner()
	winners.append(winner)

print(winners.count('Player 1'))
print(winners.count('Player 2'))
