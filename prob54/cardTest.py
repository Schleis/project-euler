import unittest
import poker

hands = [['5H', '5C', '6S', '7S', 'KD', '2C', '3S', '8S', '8D', 'TD'], ['5D','8C','9S','JS','AC','2C','5C','7D','8S','QH'],['2D','9C','AS','AH','AC','3D','6D','7D','TD','QD'],['4D','6S','9H','QH','QC','3D','6D','7H','QD','QS'],['2H','2D','4C','4D','4S','3C','3D','3S','9S','9D'], ['KC','7H','QC','6D','8H','6S','5S','AH','7S','8C']]
winners=['Player 2', 'Player 1', 'Player 2', 'Player 1', 'Player 1', 'Player 2']

class GameTest(unittest.TestCase):
	def testGameConstruct(self):
		for hand in hands:
			myGame = poker.Game(hand)
			hand1 = hand[0:5]
			hand2 = hand[5:]
			self.assertEquals(hand1, myGame.player1.getHand())
			self.assertEquals(hand2, myGame.player2.getHand())

class HandTest(unittest.TestCase):
	handData = [['5H', '5C', '6S', '7S', 'KD'],['2C', '3S', '8S', '8D', 'TD'], ['5D','8C','9S','JS','AC'],['2C','5C','7D','8S','QH'],['2D','9C','AS','AH','AC'],['3D','6D','7D','TD','QD'],['4D','6S','9H','QH','QC'],['3D','6D','7H','QD','QS'],['2H','2D','4C','4D','4S'],['3C','3D','3S','9S','9D'], ['5H','6C','7D','8S','9H'],['4H','5H', '7H','8H','6H'], ['6D', '6C','TD','TH','KD']]
	ranking = ['One Pair', 'One Pair', 'High Card', 'High Card', 'Three of a Kind', 'Flush', 'One Pair', 'One Pair', 'Full House', 'Full House', 'Straight', 'Straight Flush', 'Two Pair']
	def testHandRanking(self):
		for hand in range(0, len(self.handData)):
			myHand = poker.Hand(self.handData[hand])
			self.assertEquals(self.ranking[hand], myHand.determineHandRanking())

class PokerTest(unittest.TestCase):
	def testPokerGames(self):
		for hand in range(0, len(hands)):
			pokerGame = poker.Game(hands[hand])
			winner = pokerGame.getWinner()
			print(winner)
			self.assertEquals(winners[hand], winner)
		

if __name__ == "__main__":
	unittest.main()
