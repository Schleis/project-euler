handRankings = {'High Card' : 0, 'One Pair' : 1, 'Two Pair' : 2, 'Three of a Kind' : 3, 'Straight' : 4, 'Flush' : 5, 'Flush' : 6, 'Full House' : 7, 'Four of a Kind' : 8, 'Straight Flush' : 9}

cardRankings = {'2':0, '3': 1, '4':2, '5':3, '6':4, '7':5, '8':6, '9':7, 'T':8, 'J':9, 'Q':10, 'K':11, 'A':12}

class Game:
	def __init__(self, hands):
		self.player1 = Hand(hands[0:5])
		self.player2 = Hand(hands[5:])
	
	def getWinner(self):
		player1Hand = self.player1.determineHandRanking()
		player2Hand = self.player2.determineHandRanking()
		if (handRankings[player1Hand] < handRankings[player2Hand]):
			winner = 'Player 2'
		elif (handRankings[player1Hand] > handRankings[player2Hand]): 
			winner = 'Player 1'
		else:
			winner = self.__compareHighCards()
		print('Player 1 has : ' + player1Hand)
		print(self.player1.getHand())
		print('Player 2 has : ' + player2Hand)
		print(self.player2.getHand())
		print(winner)
		return winner
	
	def __compareHighCards(self):
		player1CardSets = {}
		for k,v in self.player1.getHighCards().iteritems():
			if v not in player1CardSets.keys():
				player1CardSets[v] = []
			player1CardSets[v].append(k)
		
		player2CardSets = {}
		for k,v in self.player2.getHighCards().iteritems():
			if v not in player2CardSets.keys():
				player2CardSets[v] = []
			player2CardSets[v].append(k)

		cardKeys = sorted(player1CardSets.keys(), reverse=True)
		
		for cardKey in cardKeys:
			player1Set = sorted(player1CardSets[cardKey], reverse=True)
			player2Set = sorted(player2CardSets[cardKey], reverse=True)
			print(player1Set)
			print(player2Set)
			for card in range(0, len(player1Set)):
				if (player1Set[card] < player2Set[card]):
					return 'Player 2'
				elif (player1Set[card] > player2Set[card]):
					return 'Player 1'	

class Hand:
	def __init__(self, cards):
		self.cards = cards

	def getHand(self):
		return self.cards

	def determineHandRanking(self):
		faces = self.__getCardFaces()
		suits = self.__getCardSuits()
		isFlush = self.__isFlush(suits)
		isStraight = self.__isStraight(faces)
		numberOfFaces = self.__getNumberOfFaces(faces)

		if (isFlush and isStraight):
			return 'Straight Flush'
		elif (isFlush):
			return 'Flush'
		elif (isStraight):
			return 'Straight'
		if(2 == len(numberOfFaces)):
			for setOfFaces in numberOfFaces:
				if(2 == numberOfFaces[setOfFaces] or 3 == numberOfFaces[setOfFaces]):
					return 'Full House'
				if(4 == numberOfFaces[setOfFaces]):
					return 'Four of a Kind'
		if(3 == len(numberOfFaces)):
			for setOfFaces in numberOfFaces:
				if(2 == numberOfFaces[setOfFaces]):
					return 'Two Pair'

		for setOfFaces in numberOfFaces:
			if(2 == numberOfFaces[setOfFaces]):
				return 'One Pair'
			if(3 == numberOfFaces[setOfFaces]):
				return 'Three of a Kind'
		return 'High Card'

	def getHighCards(self):
		faces = self.__getCardFaces()
		return self.__getNumberOfFaces(faces)

	def __getNumberOfFaces(self, faces):
		facesNumber = {}
		for card in faces:
			facesNumber[card] = faces.count(card)
		return facesNumber
	
	def __getCardFaces(self):
		faces = []
		for card in self.cards:
			faces.append(cardRankings[card[0:1]])
		return faces
	
	def __getCardSuits(self):
		suits = []
		for card in self.cards:
			suits.append(card[1:])
		return suits

	def __isFlush(self, suits):
		for suit in suits:
			if (suit != suits[0]):
				return False
		return True
	
	def __isStraight(self, faces):
		faces.sort()
		for card in range(1, len(faces)):
			difference = faces[card] - faces[card-1]
			if(1 != difference):
				return False
		return True
