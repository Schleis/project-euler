class primes:
	def __init__(self, primeListLimit = 1000000):
		"""Initialize a large list of primes for primality checks"""
		self.primeList = self.sieveErastothene(primeListLimit)
		self.primeLimit = primeListLimit
	
	def isPrime(self, number):
		if(self.primeLimit > number):
			"""If the number is beloew the limit, check to see if it is in the list"""
			return (number in self.primeList)
		else:
			"""Number is larger than the limit, so check for prime factors"""
			for p in self.primeList:
				if p**2 > number:
					return True
				else:
					if (number % p == 0):
						return False

	def sieveErastothene(self, limit):
		"""Use the Sieve of Erastothene to return a list of all the prime numbers below the limit

		Returns a list
		limit is an integer"""

		n = range(3,limit,2)
		p = [2]

		for a in range(0,len(n)):
			if(n[a] != 0):
				p.append(n[a])
				step = n[a]
				for b in range(a,len(n),step):
					n[b] = 0
		return p
